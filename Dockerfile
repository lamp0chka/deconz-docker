FROM ubuntu:xenial
MAINTAINER appserv.nl

EXPOSE 3389
EXPOSE 8080

ENV DECONZ_MAJOR_VERSION 2
ENV DECONZ_MINOR_VERSION 05
ENV DECONZ_BUILD_VERSION 01

VOLUME ["/root/.local/share/dresden-elektronik/deCONZ"]

RUN usermod -a -G dialout root \
# && echo 'APT::Install-Recommends 0;' >> /etc/apt/apt.conf.d/01norecommends \
# && echo 'APT::Install-Suggests 0;' >> /etc/apt/apt.conf.d/01norecommends \
 && apt-get update \
&& DEBIAN_FRONTEND=noninteractive apt-get install -y \
   build-essential \
   git \
   vim.tiny \
   wget \
   sudo \
   net-tools \
   sqlite3 \
   ca-certificates \
   unzip \
   apt-transport-https \
   qt5-default \
   libqt5sql5 \
   libqt5websockets5-dev \
   libqt5serialport5-dev \
 && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* 
	
RUN wget https://www.dresden-elektronik.de/deconz/ubuntu/beta/deconz-${DECONZ_MAJOR_VERSION}.${DECONZ_MINOR_VERSION}.${DECONZ_BUILD_VERSION}-qt5.deb \
 && wget https://www.dresden-elektronik.de/deconz/ubuntu/beta/deconz-dev-${DECONZ_MAJOR_VERSION}.${DECONZ_MINOR_VERSION}.${DECONZ_BUILD_VERSION}.deb \
 && dpkg -i deconz-${DECONZ_MAJOR_VERSION}.${DECONZ_MINOR_VERSION}.${DECONZ_BUILD_VERSION}-qt5.deb \
 && dpkg -i deconz-dev-${DECONZ_MAJOR_VERSION}.${DECONZ_MINOR_VERSION}.${DECONZ_BUILD_VERSION}.deb \
 && git clone https://github.com/dresden-elektronik/deconz-rest-plugin.git \
 && cd deconz-rest-plugin \
 && git checkout -b mybranch V${DECONZ_MAJOR_VERSION}_${DECONZ_MINOR_VERSION}_${DECONZ_BUILD_VERSION} \
 && qmake \
 && make -j2 \
 && cp ../libde_rest_plugin.so /usr/share/deCONZ/plugins


RUN chown root:root /usr/bin/deCONZ*
RUN mkdir /root/otau
RUN mkdir -p /run/udev/data/ && \
echo "E:ID_VENDOR_ID=0403\nE:ID_MODEL_ID=6015" > /run/udev/data/c188:0

CMD /usr/bin/deCONZ --auto-connect=1 -platform minimal --upnp=0 --http-port=8080 --dbg-error=1
